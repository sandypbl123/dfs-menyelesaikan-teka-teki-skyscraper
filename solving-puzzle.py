from _typeshed import Self
# Kelompok Tugas Kecerdasan Tiruan
# Fazrin Tri Wahyuni
# Sandy Ardiansyah
# Yudi Dwiyanto
# Budimantoro
# M. Ikhsan Kamil
class Board:
    def __init__(self,size):
        self.board = [{i + 1 for i in range(size)} for j in range(size * size)]
        self.size = size
class Skyscrapper:
    def __init__(self, size:int, clues=None):
        self.board = Board(size)
        self.clues = clues if clues else [0 for i in range(size * size)]
        self.size = size

for i in cell_indices:
	if clue == 1 or clue == self.size:
		self.resolve(i, last)
	else:
		self.exclude(i, exclude)
		exclude.add(last)
	last -= 1

def resolve(self, i, last: int):
    self.board[i] = {last}
    self.propagate_constraint(i)

def exclude(self, idx, exclude):
    for num in iter(exclude):
        if num in self.board[idx]:
            self.board[idx] -= {num}
            self.eliminate(self.get_row_from_cell_index(idx), num)
            self.eliminate(self.get_col_from_cell_index(idx), num)

def propagate_constraint(self, idx) -> None:
    num = self.board[idx]
    for direction in Direction:
        for i in self.get_indices(idx, direction):
            if i != idx:
                self.exclude(i, num)

def try_resolve_board(self):
	for i in range(self.size):
		self.try_resolve_row(i)
		self.try_resolve_col(i)

def try_resolve_row(self, row: int):
	clue_idx = row + self.size
	self.resolve_helper(clue_idx, self.get_indices_from_clue_all(clue_idx))

def try_resolve_col(self, col: int):
	self.resolve_helper(col, self.get_indices_from_clue_all(col))

def resolve_helper(self, clue_idx, sequences):
	clue1 = self.clues[clue_idx]
	clue2 = self.clues[self.get_opposite_clue_index(clues_idx)]
	possibles = [sequence for sequence in self.get_all_possible_sequences(sequences)
			if len(set(sequence)) == 4 and self.check_clue(sequence, clue1)
			and self.check_clue(reversed(sequence), clue2)]
	ans = [set() for x in range(self.size)]
	for possible in possibles:
		for i, x in enumerate(possible):
			ans[i].add(x)
	if all(ans):
		for i, idx in enumerate(self.get_indices_form_clue_all(clue_idx)):
			self.board[idx] = ans[i]
			if len(ans[i]) == 1:
				self.propagate_constraint(idx)

def get_all_possible_sequences(self, indices):
	return producy(*map(lambda x: list(self.board[x]), indices))

def get_indices(self, start : int, direction: Direction) -> iter :
	if not self.valid_index(start):
		return range(-1)
	idx_range = {
		Direction.up: range(start, -1, -self.size),
		Direction.right: range(start, (start // self.size + 1) * self.size),
		Direction.down: range(start, self.size * self.size,self.size),
		Direction.left: range(start, start // self.size * self.size - 1, -1),
	}[direction]
	return inter(idx_range)

def get_row_from_cell_index(self, cell_idx):
	return self.get_inter_from_cell_index(cell_idx, [Direction.left, Direction.right])

def get_col_from_cell_index(self, cell_idx):
	return self.get_iter_from_cell_index(cell_idx, [Direction.up, Direction.down])

board1 = Skyscraper(4, clues=[0,0,1,2,0,2,0,0,0,3,0,0,0,1,0,0])
t1 = time.time()
board.solve()
printf((time.time()- ti) / 1000, "ms")
printf(board1)
